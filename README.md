OpenChimera
===
___

Requirements
---

Some libraries are needed to run the **OpenChimera** library.
All of them are optional and you can adjust CMake to suit your needs.

* [OpenNI2](http://www.openni.org/openni-sdk/) & [NITE2](http://www.openni.org/files/nite/)
* [psmoveapi](http://thp.io/2010/psmove/)
* [Oculus Rift SDK](https://developer.oculusvr.com/)

Your PATH now needs to be set up correctly.

Configure your environment
--

To make OpenChimera work, you need to set up your PATH according to your installation.
You need to add **OPENNI2**, **NITE2**, **OCULUS** and **PSMOVEAPI** to your environment.

**Example** *(use your own directories!)* :

        OPENNI2 = C:\Program Files (x86)\OpenNI2
        NITE2 = C:\Program Files (x86)\PrimeSense\NiTE2
        OCULUS = D:\SDKs\OculusSDK
        PSMOVEAPI = D:\SDKs\psmoveapi

The library is almost ready to build now
---
But before, you need to use **cmake** to configure the library according to your installation and configuration.

To do
--
* Add cmake and make it the more configurable possible
* Complete this file