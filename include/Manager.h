#ifndef __OPEN_CHIMERA_MANAGER
#define __OPEN_CHIMERA_MANAGER

#include <list>
#include <iostream>
#include "Device.h"
#include "devices/DevicePSMove.h"

namespace OpenChimera {
	class Manager
	{
	protected:

	public:
		Manager();
		~Manager();

		virtual std::list<Device *> getDevices(enum DeviceType = eAll);
		virtual void initDevices(std::list<Device *> list);
	};
}

#endif