#ifndef __OPEN_CHIMERA_DEVICE
#define __OPEN_CHIMERA_DEVICE

#include <string>

namespace OpenChimera {
	enum DeviceType {
		eNone = 0x0,
		eOculus = 0x1,
		ePSMove = 0x2,
		eKinect = 0x4,
		eAll = eOculus | ePSMove | eKinect,
	};

	class Device {
	protected:
		enum DeviceType type;

	public:
		virtual enum DeviceType getType() { return type; };
		virtual std::string getName() = 0;
		virtual void init() = 0;
	};
}

#endif