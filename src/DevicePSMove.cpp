#include "devices/DevicePSMove.h"

namespace OpenChimera {

	std::list<Device *> DevicePSMove::detectPSMove(bool allowUSB)
	{
		std::list<Device *> out;
		PSMove *tmp;
		int i = 0;

		int count = DevicePSMove::countConnected();
		if (count <= 0)
			return out;

		while ((tmp = psmove_connect_by_id(i)))
		{
			if (allowUSB || psmove_connection_type(tmp) == Conn_Bluetooth)
			{
				out.push_back(new DevicePSMove(tmp));
			}
			++i;
		}

		return out;
	}

	DevicePSMove::~DevicePSMove()
	{
		this->disconnect();
	}

	DevicePSMove::DevicePSMove(PSMove *ptr)
		: ptr(ptr)
	{
		type = ePSMove;
	}

	PSMove *DevicePSMove::getPtr()
	{
		return ptr;
	}

	enum DeviceType DevicePSMove::getType()
	{
		return type;
	}

	std::string DevicePSMove::getName()
	{
		return "DevicePSMove controller";
	}

	void DevicePSMove::init()
	{
		psmove_enable_orientation(ptr, PSMove_True);
	}

	void DevicePSMove::setRemoteConfig( enum PSMove_RemoteConfig config )
	{
		psmove_set_remote_config(config);
	}

	int DevicePSMove::countConnected()
	{
		return psmove_count_connected();
	}

	PSMove * DevicePSMove::connect()
	{
		return psmove_connect();
	}

	PSMove * DevicePSMove::connectById( int id )
	{
		return psmove_connect_by_id(id);
	}

	enum PSMove_Connection_Type DevicePSMove::connectionType()
	{
		return psmove_connection_type(ptr);
	}

	bool DevicePSMove::isRemote()
	{
		return (bool)psmove_is_remote(ptr);
	}

	char * DevicePSMove::getSerial()
	{
		return psmove_get_serial(ptr);
	}

	bool DevicePSMove::pair()
	{
		return (bool)psmove_pair(ptr);
	}

	bool DevicePSMove::pairCustom( const char *btaddr_string )
	{
		return (bool)psmove_pair_custom(ptr, btaddr_string);
	}

	void DevicePSMove::setRateLimiting( bool enabled )
	{
		psmove_set_rate_limiting(ptr, (PSMove_Bool)enabled);
	}

	void DevicePSMove::setLeds( unsigned char r, unsigned char g, unsigned char b )
	{
		psmove_set_leds(ptr, r, g, b);
	}

	void DevicePSMove::setRumble( unsigned char rumble )
	{
		psmove_set_rumble(ptr, rumble);
	}

	enum PSMove_Update_Result DevicePSMove::updateLeds()
	{
		return psmove_update_leds(ptr);
	}

	int DevicePSMove::poll()
	{
		return psmove_poll(ptr);
	}

	unsigned int DevicePSMove::getButtons()
	{
		return psmove_get_buttons(ptr);
	}

	void DevicePSMove::getButtonEvents( unsigned int *pressed, unsigned int *released )
	{
		psmove_get_button_events(ptr, pressed, released);
	}

	enum PSMove_Battery_Level DevicePSMove::getBattery()
	{
		return psmove_get_battery(ptr);
	}

	int DevicePSMove::getTemperature()
	{
		return psmove_get_temperature(ptr);
	}

	unsigned char DevicePSMove::getTrigger()
	{
		return psmove_get_trigger(ptr);
	}

	void DevicePSMove::getAccelerometer( int *ax, int *ay, int *az )
	{
		psmove_get_accelerometer(ptr, ax, ay, az);
	}

	void DevicePSMove::getGyroscope( int *gx, int *gy, int *gz )
	{
		psmove_get_gyroscope(ptr, gx, gy, gz);
	}

	void DevicePSMove::getMagnetometer( int *mx, int *my, int *mz )
	{
		psmove_get_magnetometer(ptr, mx, my, mz);
	}

	void DevicePSMove::getAccelerometerFrame( enum PSMove_Frame frame, float *ax, float *ay, float *az )
	{
		psmove_get_accelerometer_frame(ptr, frame, ax, ay, az);
	}

	void DevicePSMove::getGyroscopeFrame( enum PSMove_Frame frame, float *gx, float *gy, float *gz )
	{
		psmove_get_gyroscope_frame(ptr, frame, gx, gy, gz);
	}

	void DevicePSMove::getMagnetometerVector( float *mx, float *my, float *mz )
	{
		psmove_get_magnetometer_vector(ptr, mx, my, mz);
	}

	bool DevicePSMove::hasCalibration()
	{
		return (bool)psmove_has_calibration(ptr);
	}

	void DevicePSMove::dumpCalibration()
	{
		psmove_dump_calibration(ptr);
	}

	void DevicePSMove::enableOrientation( bool enabled, bool reset )
	{
		psmove_enable_orientation(ptr, (PSMove_Bool)enabled);

		if (reset == true)
			this->resetOrientation();
	}

	bool DevicePSMove::hasOrientation()
	{
		return (bool)psmove_has_orientation(ptr);
	}

	void DevicePSMove::getOrientation( float *w, float *x, float *y, float *z )
	{
		psmove_get_orientation(ptr, w, x, y, z);
	}

	void DevicePSMove::resetOrientation()
	{
		psmove_reset_orientation(ptr);
	}

	void DevicePSMove::resetMagnetometerCalibration()
	{
		psmove_reset_magnetometer_calibration(ptr);
	}

	void DevicePSMove::saveMagnetometerCalibration()
	{
		psmove_save_magnetometer_calibration(ptr);
	}

	int DevicePSMove::getMagnetometerCalibrationRange()
	{
		return psmove_get_magnetometer_calibration_range(ptr);
	}

	void DevicePSMove::disconnect()
	{
		psmove_disconnect(ptr);
	}

	void DevicePSMove::reinit()
	{
		psmove_reinit();
	}

	long DevicePSMove::utilGetTicks()
	{
		return psmove_util_get_ticks();
	}

	const char * DevicePSMove::utilGetDataDir()
	{
		return psmove_util_get_data_dir();
	}

	char * DevicePSMove::utilGetFilePath( const char *filename )
	{
		return psmove_util_get_file_path(filename);
	}

	int DevicePSMove::utilGetEnvInt( const char *name )
	{
		return psmove_util_get_env_int(name);
	}

	char * DevicePSMove::utilGetEnvString( const char *name )
	{
		return psmove_util_get_env_string(name);
	}

}