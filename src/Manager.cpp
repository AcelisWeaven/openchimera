#include <iostream>
#include "OpenChimera.h"

namespace OpenChimera {

	Manager::Manager()
	{
		std::cout << "OpenChimera::Manager created" << std::endl;
	}

	Manager::~Manager()
	{
		std::cout << "OpenChimera::Manager destroyed" << std::endl;
	}

	std::list<Device *> Manager::getDevices(enum DeviceType t)
	{
		std::list<Device *> out;

		if (t & ePSMove)
			out.splice(end(out), DevicePSMove::detectPSMove());

		return out;
	};

	void Manager::initDevices(std::list<Device *> list)
	{
		for (auto it = begin(list) ; it != end(list) ; ++it)
		{
			(*it)->init();
		}
	}


}