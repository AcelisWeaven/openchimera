###############################################################################
# Find OpenNI2
#
# This sets the following variables:
# OPENNI2_FOUND - True if OPENNI was found.
# OPENNI2_INCLUDE_DIRS - Directories containing the OPENNI include files.
# OPENNI2_LIBRARIES - Libraries needed to use OPENNI.
#
# Edited from: https://github.com/rgbdemo/nestk/


set(OPENNI2_DEFINITIONS ${PC_OPENNI_CFLAGS_OTHER})

find_path(OPENNI2_INCLUDE_DIR OpenNI.h
          HINTS
			${PC_OPENNI_INCLUDEDIR}
			${PC_OPENNI_INCLUDE_DIRS}
			/usr/include/openni2
			/usr/include/ni2
          PATHS
			"$ENV{PROGRAMFILES}/OpenNI2/Include"
			"$ENV{PROGRAMW6432}/OpenNI2/Include"
			"$ENV{OPENNI2}/Include"
          PATH_SUFFIXES openni ni
		  )

find_library(OPENNI2_LIBRARY
             NAMES OpenNI2
             HINTS
				${PC_OPENNI_LIBDIR}
				${PC_OPENNI_LIBRARY_DIRS}
				/usr/lib
             PATHS
				"$ENV{PROGRAMFILES}/OpenNI2/Redist"
				"$ENV{PROGRAMW6432}/OpenNI2/Redist"
				"$ENV{PROGRAMW6432}/OpenNI2"
				"$ENV{PROGRAMFILES}/OpenNI2/Lib"
				"$ENV{PROGRAMW6432}/OpenNI2/Lib"
				"$ENV{OPENNI2}/Lib"
             PATH_SUFFIXES lib lib64
)

set(OPENNI2_INCLUDE_DIRS ${OPENNI2_INCLUDE_DIR})
set(OPENNI2_LIBRARIES ${OPENNI2_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(OpenNI2 DEFAULT_MSG
    OPENNI2_LIBRARY OPENNI2_INCLUDE_DIR)

mark_as_advanced(OPENNI2_LIBRARY OPENNI2_INCLUDE_DIR)
if(OPENNI2_FOUND)
  include_directories(${OPENNI2_INCLUDE_DIRS})
  message(STATUS "OpenNI2 found (include: ${OPENNI2_INCLUDE_DIR}, lib: ${OPENNI2_LIBRARY})")
endif(OPENNI2_FOUND)

