###############################################################################
# Find PsMoveApi
#
# This sets the following variables:
# PSMOVEAPI_FOUND - True if PsMoveApi was found.
# PSMOVEAPI_INCLUDE_DIRS - Directories containing the PsMoveApi include files.
# PSMOVEAPI_LIBRARIES - Libraries needed to use PsMoveApi.

find_path(PSMOVEAPI_INCLUDE_DIR psmove.h
          PATHS
			"$ENV{PSMOVE}/include"
			"$ENV{PSMOVEAPI}/include"
		  )

find_library(PSMOVEAPI_LIBRARY
             NAMES psmoveapi
             PATHS
				"$ENV{PSMOVE}"
				"$ENV{PSMOVEAPI}"
)

set(PSMOVEAPI_INCLUDE_DIRS ${PSMOVEAPI_INCLUDE_DIR})
set(PSMOVEAPI_LIBRARIES ${PSMOVEAPI_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(PsMoveApi DEFAULT_MSG
    PSMOVEAPI_LIBRARY PSMOVEAPI_INCLUDE_DIR)

mark_as_advanced(PSMOVEAPI_LIBRARY PSMOVEAPI_INCLUDE_DIR)
if(PSMOVEAPI_FOUND)
  include_directories(${PSMOVEAPI_INCLUDE_DIRS})
  message(STATUS "PsMoveApi found (include: ${PSMOVEAPI_INCLUDE_DIR}, lib: ${PSMOVEAPI_LIBRARY})")
endif(PSMOVEAPI_FOUND)

