###############################################################################
# Find NITE2
#
# This sets the following variables:
# NITE2_FOUND - True if NiTE was found.
# NITE2_INCLUDE_DIRS - Directories containing the NiTE include files.
# NITE2_LIBRARIES - Libraries needed to use NiTE.

find_path(NITE2_INCLUDE_DIR NiTE.h
          HINTS
			#TODO: Check paths on linux
			/usr/include/NITE2
          PATHS
			"$ENV{PROGRAMFILES}/PrimeSense/NiTE2/Include"
			"$ENV{PROGRAMW6432}/PrimeSense/NiTE2/Include"
			"$ENV{NITE2}/Include"
          PATH_SUFFIXES nite ni
		  )

find_library(NITE2_LIBRARY
             NAMES NiTE2
             HINTS
				/usr/lib
             PATHS
				"$ENV{PROGRAMFILES}/PrimeSense/NiTE2/Redist"
				"$ENV{PROGRAMW6432}/PrimeSense/NiTE2/Redist"
				"$ENV{PROGRAMFILES}/PrimeSense/NiTE2"
				"$ENV{PROGRAMW6432}/PrimeSense/NiTE2"
				"$ENV{PROGRAMFILES}/PrimeSense/NiTE2/Lib"
				"$ENV{PROGRAMW6432}/PrimeSense/NiTE2/Lib"
				"$ENV{NITE2}/Lib"
             PATH_SUFFIXES lib lib64
)

set(NITE2_INCLUDE_DIRS ${NITE2_INCLUDE_DIR})
set(NITE2_LIBRARIES ${NITE2_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(NITE2 DEFAULT_MSG
    NITE2_LIBRARY NITE2_INCLUDE_DIR)

mark_as_advanced(NITE2_LIBRARY NITE2_INCLUDE_DIR)
if(NITE2_FOUND)
  include_directories(${NITE2_INCLUDE_DIRS})
  message(STATUS "NITE2 found (include: ${NITE2_INCLUDE_DIR}, lib: ${NITE2_LIBRARY})")
endif(NITE2_FOUND)

