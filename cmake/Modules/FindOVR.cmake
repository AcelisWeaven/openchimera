###############################################################################
# Find OVR
#
# This sets the following variables:
# OVR_FOUND - True if OVR was found.
# OVR_INCLUDE_DIRS - Directories containing the OVR include files.
# OVR_LIBRARIES - Libraries needed to use OVR.

find_path(OVR_INCLUDE_DIR OVR.h
          PATHS
			"$ENV{OCULUS}/Include"
		  )

find_library(OVR_LIBRARY
             NAMES libovr
             PATHS
				#TODO: Add linux paths
				"$ENV{OCULUS}/Lib"
             PATH_SUFFIXES Win32 x64
)

set(OVR_INCLUDE_DIRS ${OVR_INCLUDE_DIR})
set(OVR_LIBRARIES ${OVR_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(OVR DEFAULT_MSG
    OVR_LIBRARY OVR_INCLUDE_DIR)

mark_as_advanced(OVR_LIBRARY OVR_INCLUDE_DIR)
if(OVR_FOUND)
  include_directories(${OVR_INCLUDE_DIRS})
  message(STATUS "OVR found (include: ${OVR_INCLUDE_DIR}, lib: ${OVR_LIBRARY})")
endif(OVR_FOUND)

