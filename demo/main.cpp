#include <iostream>
#include "openchimera.h"

using namespace OpenChimera;

void handlePSMoveControls(DevicePSMove *p)
{
	unsigned int pressed, stillPressed, released;
	float w, x, y, z;

	if (p->poll()) // Is data available ?
	{
		p->getButtonEvents(&pressed, &released);
		stillPressed = p->getButtons();

		if (pressed & Btn_CROSS)
		{
			p->resetOrientation();
		}
		if (stillPressed & Btn_CIRCLE)
		{
			p->getOrientation(&w, &x, &y, &z);
			std::cout << "[ w: " << w << " x: " << x << " y: " << y << " z: " << z << "]" << std::endl;

			p->setLeds(255, 255, 255);
			p->updateLeds();
		}
	}
}

int main()
{
	Manager *m = new Manager();

	std::list<Device *> devices = m->getDevices();
	m->initDevices(devices);
	for (auto it = begin(devices) ; it != end(devices) ; ++it)
	{
		std::cout << (*it)->getName() << std::endl;
		auto p = dynamic_cast<DevicePSMove *>(*it);
		if (p)
		{
			p->setLeds(0, 255, 255);
			p->updateLeds();
			p->enableOrientation();
		}
	}

	while (true)
	{
		for (auto it = begin(devices) ; it != end(devices) ; ++it)
		{
			auto p = dynamic_cast<DevicePSMove *>(*it);
			if (p)
				handlePSMoveControls(p);
		}
		Sleep(5); // Not the best way to deal with events...
	}

	return 0;
}